/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package analizadorsintactico;

import static java.lang.Math.pow;

/**
 *
 * @author Usuario
 */
public class Utilidades {

    public static int typeBase(Object base) {
        System.out.println(base.getClass().getSimpleName());
        if (base.getClass().equals(String.class)) {
            System.out.println("String");
            return 1;
        } else if (base.getClass().equals(Integer.class)) {
            System.out.println("Integer");
            return 2;
        } else if (base.getClass().equals(Double.class)) {
            System.out.println("Double");
            return 3;
        } else {
            return -1;
        }
    }

    public static boolean isEquals(Object base1, Object base2) {
        return base2.toString().equals(base1.toString());

    }

    public static void ResolverR1(Object base1, Object base2, Object expo1, Object expo2, Object t) {
        Double Resultado = 0.0;
  
        if (isEquals(base1, base2)) {    
            
            
            
            switch (typeBase(base1)) {
                
                case 1: 
                    if (t.toString().charAt(0) == '*') {
                        Double exponente = (Double)expo1 +(Double) expo2;
                        System.out.println("Multiplicación de Potencias con la misma Base \n ");
                        System.out.println("El Resultado es: " + base1 + "^" + exponente);
                    } else if (t.toString().charAt(0) == '/') {
                        Double exponente = (Double) expo1 - (Double) expo2;
                        System.out.println("DIVISION de Potencias con la misma Base \n ");
                        System.out.println("El Resultado es: " + base1 + "^" + exponente);
                    }
                break;
                
                case 2 : 
                    if (t.toString().charAt(0) == '*') {
                        Double exponente = (Double) expo1 + (Double) expo2;
                        Resultado = pow((Double) base1, exponente);
                        System.out.println("Multiplicacion de Potencias con la misma Base \n ");
                        System.out.println("El Resultado es: " + base1 + "^" + exponente + " = " + Resultado);
                    } else if (t.toString().charAt(0) == '/') {
                        Double exponente = (Double) expo1 - (Double) expo2;
                        Resultado = pow((Double) base1, exponente);
                        System.out.println("DIVISION de Potencias con la misma Base \n ");
                        System.out.println("El Resultado es: " + base1 + "^" + exponente + " = " + Resultado);
                    }
                break;
                
                case 3 :
                    
                    if (t.toString().charAt(0) == '*') {
                        System.err.println("entro aqui ");
//                        Double exponente = Double.parseDouble(expo1.toString)+ Double.parseDouble(expo2.toString);
                        Double es=(Double)expo1+(Double)expo2;
                        System.err.println("expo  "+es);
                        Resultado = pow((Double) base1, es);
                        System.out.println("Multiplicacion de Potencias con la misma Base \n ");
                        System.out.println("El Resultado es: " + base1 + "^" + es + " = " + Resultado);
                    } else if (t.toString().charAt(0) == '/') {
                        System.out.println("INgreso a la opcion 2 ");
                        Double exponente = (Double) expo1 - (Double) expo2;
                        Resultado = pow((Double) base1, exponente);
                        System.out.println("DIVISION de Potencias con la misma Base \n ");
                        System.out.println("El Resultado es: " + base1 + "^" + exponente + " = " + Resultado);
                    }
                break;
                
                default :
                    throw new AssertionError();

            }
        } else {
            System.out.println("Las bases no son iguales");
        }
    }
    
    
    public static void regla239(Object exponente, Object base){
        Double Resultado = 0.0;
        Double base1 = (Double) base;

        if ((int) exponente == 1) {
            Double resultado1 = Double.valueOf(base1);
            System.out.println("El Resultado es: " + base1 + "^" + exponente + " = " + resultado1);

        } else if ((int) exponente == 0) {
            Resultado = 1.0;
            System.out.println("El Resultado es: " + base1 + "^" + exponente + " = " + Resultado);

        } else if ((int) exponente == -1) {
            Double resultado2 = 1/Double.valueOf(base1);
            
            System.out.println("El Resultado es: " + base1 + "^" + exponente + " = " + "1/" + base+" = "+resultado2);

        } else {

            System.out.println("*****************ERROR**********");

        }
    }
    public static void regla4(Double a, Double e, Double x) {
        Double Resultado = 0.0;
        
        
        Double exponente = e * x;
        Resultado = pow(a, exponente);
        System.out.println("Potencia de Otra Potencia \n ");
        System.out.println("El Resultado es: " + a + "^" + e + "*" + x + "="
                //+ a + exponente + " = "
                + Resultado);
    }
//    [1*5]^2 ;
     public static void regla6_7(Object num1, Object num2, Object expo,Object t){
        Double b=(Double)num1;
        Double n=(Double)num2;
        Double d=(Double)expo;
        
        if (t.toString().charAt(0) == '*') {
                        Double Resultado=pow(b,d)*pow(n,d);
                        
                        System.out.println("PRODUCTO DE POTENCIAS  \n "+"Resultado es:  " +Resultado);
                       
                    } else if (t.toString().charAt(0) == '/') {
                        Double Resultado=pow(b,d)/pow(n,d);
                        System.out.println("DIVISION DE POTENCIAS  \n " +"Resultado es:  "+Resultado);
                    }
         
    }
     
//     regla6_7(base1, base2, expo1,t)
//     10^(1/2);
     public static void regla8(Object base, Object numerador, Object denominador){
        Double b=(Double)base;
         Double n=(Double)numerador;
         Double d=(Double)denominador;
         double num=n;
         double den=d;
         double exp=num/den;
         Double Resultado=pow(b,exp);
         
         System.out.println("Potencia  elevado a una fraccion   \n:"+"El resultado es: "+Resultado); 
     } 
}
